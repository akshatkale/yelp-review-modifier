import java.io.StringReader;
import java.util.ArrayList;
import java.util.List;

import edu.stanford.nlp.ling.CoreLabel;
import edu.stanford.nlp.ling.Sentence;
import edu.stanford.nlp.parser.lexparser.LexicalizedParser;
import edu.stanford.nlp.process.CoreLabelTokenFactory;
import edu.stanford.nlp.process.PTBTokenizer;
import edu.stanford.nlp.process.Tokenizer;
import edu.stanford.nlp.process.TokenizerFactory;
import edu.stanford.nlp.trees.GrammaticalStructure;
import edu.stanford.nlp.trees.GrammaticalStructureFactory;
import edu.stanford.nlp.trees.Tree;
import edu.stanford.nlp.trees.TreePrint;
import edu.stanford.nlp.trees.TreebankLanguagePack;
import edu.stanford.nlp.trees.TypedDependency;

public class Test {
	
 //	public static void main(String args[]){
//		
//		String parserModel = "edu/stanford/nlp/models/lexparser/englishPCFG.ser.gz";
//	    if (args.length > 0) {
//	      parserModel = args[0];
//	    }
//	    LexicalizedParser lp = LexicalizedParser.loadModel(parserModel);
//	    
//	    String phrase = "they have awesome chicken tikka masala.";
//
//	    if (args.length == 0) {
//	      demoAPI(lp,phrase);
//	    } else {
//	      String textFile = (args.length > 1) ? args[1] : args[0];
//	      demoDP(lp, textFile);
//	    }
//		
//	}
	
	
	
	static List<Tree> verbList=new ArrayList<Tree>();
    static List<Tree> pnounList=new ArrayList<Tree>();
    
	public static List<Tree> getVerbList() {
		return verbList;
	}


	public static List<Tree> getPnounList() {
		return pnounList;
	}


	public void initiate(String args[], String phrase){
		String parserModel = "edu/stanford/nlp/models/lexparser/englishPCFG.ser.gz";
	    if (args.length > 0) {
	      parserModel = args[0];
	    }
	    LexicalizedParser lp = LexicalizedParser.loadModel(parserModel);

	    if (args.length == 0) {
	      demoAPI(lp, phrase);
	    } else {
	      String textFile = (args.length > 1) ? args[1] : args[0];
	      demoDP(lp, textFile);
	    }
	}
	
	
	public static void demoDP(LexicalizedParser lp, String filename) {
		
	}
	
public static void demoAPI(LexicalizedParser lp, String phrase){
		
		
		String[] sent = { "This", "is", "an", "easy", "sentence", "." };
		List<CoreLabel> rawWords = Sentence.toCoreLabelList(sent);
	    Tree parse = lp.apply(rawWords);
	    // parse.pennPrint();
	    // System.out.println();
		
		String sent2 = phrase;
	    TokenizerFactory<CoreLabel> tokenizerFactory =
	        PTBTokenizer.factory(new CoreLabelTokenFactory(), "");
	    Tokenizer<CoreLabel> tok =
	        tokenizerFactory.getTokenizer(new StringReader(sent2));
	    List<CoreLabel> rawWords2 = tok.tokenize();
	    parse = lp.apply(rawWords2);

	    TreebankLanguagePack tlp = lp.treebankLanguagePack(); // PennTreebankLanguagePack for English
	    GrammaticalStructureFactory gsf = tlp.grammaticalStructureFactory();
	    GrammaticalStructure gs = gsf.newGrammaticalStructure(parse);
	    List<TypedDependency> tdl = gs.typedDependenciesCCprocessed();
	    System.out.println(tdl);
	    System.out.println();

	    // You can also use a TreePrint object to print trees and dependencies
	    TreePrint tp = new TreePrint("penn,typedDependenciesCollapsed");
	    //tp.printTree(parse);
	    getPhrase(parse);
		
	}

	
	
//	public static void demoAPI(LexicalizedParser lp) {
//		String[] sent = { "This", "is", "an", "easy", "sentence", "." };
//	    List<CoreLabel> rawWords = Sentence.toCoreLabelList(sent);
//	    Tree parse = lp.apply(rawWords);
//	    parse.pennPrint();
//	    System.out.println();
//		
//		String sent2 = "When was the comet discovered John?";
//	    TokenizerFactory<CoreLabel> tokenizerFactory =
//	        PTBTokenizer.factory(new CoreLabelTokenFactory(), "");
//	    Tokenizer<CoreLabel> tok =
//	        tokenizerFactory.getTokenizer(new StringReader(sent2));
//	    List<CoreLabel> rawWords2 = tok.tokenize();
//	    parse = lp.apply(rawWords2);
//
//	    TreebankLanguagePack tlp = lp.treebankLanguagePack(); // PennTreebankLanguagePack for English
//	    GrammaticalStructureFactory gsf = tlp.grammaticalStructureFactory();
//	    GrammaticalStructure gs = gsf.newGrammaticalStructure(parse);
//	    List<TypedDependency> tdl = gs.typedDependenciesCCprocessed();
//	    System.out.println(tdl);
//	    System.out.println();
//
//	    // You can also use a TreePrint object to print trees and dependencies
//	    TreePrint tp = new TreePrint("penn,typedDependenciesCollapsed");
//	    tp.printTree(parse);
//	    getPhrase(parse);
//	}

	
	public static void getPhrase(Tree parse)
	{

//	    List<Tree> verbList=new ArrayList<Tree>();
//	    List<Tree> pnounList=new ArrayList<Tree>();
	    for (Tree subtree: parse){
	    	
	      if(subtree.label().value().equals("VB") || subtree.label().value().equals("VBN")){
	        verbList.add(subtree.lastChild());
	      }
	      if(subtree.label().value().equals("NNP") || subtree.label().value().equals("NN")){
	    	  pnounList.add(subtree.lastChild());
	      }
	    }
//	      System.out.println(verbList);
//	      System.out.println(pnounList);
	}
}
