import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.StringReader;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.LinkedHashMap;
import java.util.List;
import java.net.HttpURLConnection;
import java.net.URL;

import org.json.JSONArray;
import org.json.JSONObject;

import com.mashape.unirest.http.HttpResponse;
import com.mashape.unirest.http.JsonNode;
import com.mashape.unirest.http.Unirest;
import com.mashape.unirest.http.exceptions.UnirestException;
import com.wordsapi.www.client.*;
import com.wordsapi.www.wordsapi.api.*;
import com.wordsapi.www.wordsapi.model.*;

import edu.stanford.nlp.ling.HasWord;
import edu.stanford.nlp.ling.TaggedWord;
import edu.stanford.nlp.parser.nndep.DependencyParser;
import edu.stanford.nlp.process.DocumentPreprocessor;
import edu.stanford.nlp.tagger.maxent.MaxentTagger;
import edu.stanford.nlp.trees.GrammaticalRelation;
import edu.stanford.nlp.trees.GrammaticalStructure;
import edu.stanford.nlp.trees.TypedDependency;

/**
 * Demonstrates how to first use the tagger, then use the NN dependency
 * parser. Note that the parser will not work on untagged text.
 *
 * @author Jon Gauthier
 */


class Node{

	String noun, opinion, relation, polarity;

	Node(String noun, String opinion, String relation, String polarity){
		this.noun = noun;
		this.opinion = opinion;
		this.relation = relation;
		this.polarity = polarity;
	}

	Node(String noun, String opinion, String polarity){
		this.noun = noun;
		this.opinion = opinion;
		this.polarity = polarity;
	}
}


public class DependencyParserDemo {

	List<String> nounList = new ArrayList<>();

	LinkedHashMap<String, Integer> nounMap = new LinkedHashMap<>();

	// List<Node> nodeList = new ArrayList<Node>();

	HashSet<String> foodset = new HashSet<>();
	HashSet<String> serviceset = new HashSet<>();
	HashSet<String> ambienceset = new HashSet<>();

	HashMap<String, List<String>> compounds = new HashMap<>();

	List<String> compoundwords = new ArrayList<>(); 


	private static String modelPath;
	private static String taggerPath;
	MaxentTagger tagger;
	DependencyParser parser;


	DependencyParserDemo(){                // Constructor to load the model and tagger only once.

		modelPath = DependencyParser.DEFAULT_MODEL;
		taggerPath = "edu/stanford/nlp/models/pos-tagger/english-left3words/english-left3words-distsim.tagger";

		tagger = new MaxentTagger(taggerPath);
		parser = DependencyParser.loadFromModelFile(modelPath);


		// Populating service set to filter out Service related features.
		serviceset.add("server");
		serviceset.add("waiter");
		serviceset.add("waiteress");
		serviceset.add("service");
		serviceset.add("wait");
		serviceset.add("waiting");
		serviceset.add("family restaurant");
		serviceset.add("family");
		serviceset.add("hospitality");
		serviceset.add("staff");
		serviceset.add("manager");
		serviceset.add("reservation");
		serviceset.add("packed");


		// Populating ambience set

		ambienceset.add("small");
		ambienceset.add("crowded");
		ambienceset.add("lighting");
		ambienceset.add("toilet");
		ambienceset.add("toilets");
		ambienceset.add("parking");
		ambienceset.add("vibe");
		ambienceset.add("space");
		ambienceset.add("location");
		ambienceset.add("located");


		// Populating food set

		foodset.add("food");
		foodset.add("nutritious");
		foodset.add("drink");
		foodset.add("masala");
		foodset.add("tikka");
		foodset.add("kabab");
		foodset.add("naan");
		foodset.add("dal");
		foodset.add("tandoori");
		foodset.add("tandoor");
		foodset.add("legume");
		foodset.add("lentil");
		foodset.add("beans");
		foodset.add("dish");
		foodset.add("juice");
		foodset.add("soup");
		foodset.add("roti");
		foodset.add("bread");
		foodset.add("lamb");
		foodset.add("vindaloo");
		foodset.add("paneer");



		// foodset.add("");



	}

	public List<String> getNounList() {
		return nounList;
	}

	public LinkedHashMap<String, Integer> getnounMap(){
		return nounMap;
	}

	//	public List<Node> getNodeList(){
	//		return nodeList;
	//	}

	public void getDependency(String review) throws IOException {

		String text = review;

		System.out.println("*****************************************************************************");

		DocumentPreprocessor tokenizer = new DocumentPreprocessor(new StringReader(text));

		List<Node> nodeList = new ArrayList<Node>();

		for (List<HasWord> sentence : tokenizer) {
			List<TaggedWord> tagged = tagger.tagSentence(sentence);
			GrammaticalStructure gs = parser.predict(tagged);

			HashMap<String,Integer> nounMap = new HashMap<>();
			// List<Node> nodeList = new ArrayList<Node>();

			String tmpsentence = sentence.toString();
			tmpsentence = tmpsentence.replace(' ', '+');

			String polarity = "neutral";
			String negation = "";


			for(TaggedWord word : tagged){
				String tag = word.tag();
				if(tag.equals("NN")){

					if(!nounMap.containsKey(word.word()))
						nounMap.put(word.word(), 1);
					else
						nounMap.put(word.word(), nounMap.get(word.word())+1);
				}
			}

			System.out.println("\n\n");

			for(String noun : nounMap.keySet())
				System.out.println(noun);

			System.out.println("\n\n");


			for(TypedDependency dep : gs.typedDependenciesCollapsed()){

				String governor = dep.gov().word();
				String dependent = dep.dep().word();
				GrammaticalRelation reln = dep.reln();

				String noun = "", opinion = "";


				// Rule for amod

				if(reln.toString().equals("amod")){

					noun = dep.gov().word();
					opinion = dep.dep().word();

					// Checking for Negation

					for(TypedDependency dep2 : gs.typedDependenciesCollapsed()){


						if(dep2.reln().toString().equals("neg") && dep2.gov().word().equals(noun)){
							negation = "not";
						}

						if(dep2.dep().word().equals(noun) && dep2.reln().toString().equals("dobj")){

							String tmp = dep2.gov().word();

							for(TypedDependency dep3 : gs.typedDependenciesCollapsed()){

								if(dep3.reln().toString().equals("neg") && dep3.gov().word().equals(tmp)){
									negation = "not";
								}
							}

						}

					}


					try {
						opinion = opinion.replace(' ', '+');

						HttpResponse<JsonNode> response = Unirest.get("https://twinword-sentiment-analysis.p.mashape.com/analyze/?text=" +negation + "+" + opinion + "+" +noun)
								.header("X-Mashape-Key", "jbww4coyOHmshYmdYYBixq9DtwsYp1PgetcjsnmKRdjNTLbMQ8")
								.header("Accept", "application/json")
								.asJson();

						String result = "{ \"result\" : " + response.getBody().toString() + "}";

						JSONObject resp = new JSONObject(response.getBody().toString());

						polarity = (String) resp.get("type");

					} catch (UnirestException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					}

					Node newNode = new Node(noun.toLowerCase(), opinion.toLowerCase(), polarity);
					nodeList.add(newNode);
				}


				// Rule for nsubj

				if(reln.toString().equals("nsubj") || reln.toString().equals("advmod")){
					
					if(reln.toString().equals("nsubj")){
						noun = dep.dep().word();
						opinion = dep.gov().word();
					}
					
					else if(reln.toString().equals("advmod")){
						noun = dep.gov().word();
						opinion = dep.dep().word();
					}

					try {
						HttpResponse<JsonNode> response = Unirest.get("https://twinword-sentiment-analysis.p.mashape.com/analyze/?text=" +opinion + "+" +noun)
								.header("X-Mashape-Key", "jbww4coyOHmshYmdYYBixq9DtwsYp1PgetcjsnmKRdjNTLbMQ8")
								.header("Accept", "application/json")
								.asJson();

						String result = "{ \"result\" : " + response.getBody().toString() + "}";

						JSONObject resp = new JSONObject(response.getBody().toString());

						polarity = (String) resp.get("type");

					} catch (UnirestException e) {
						e.printStackTrace();
					}

					Node newNode = new Node(noun.toLowerCase(),opinion.toLowerCase(),polarity);
					nodeList.add(newNode);

				}


				// Rule for Compound relation

				if(reln.toString().equals("compound")){

					String noun1 = dep.gov().word();
					String noun2 = dep.dep().word();

					List<String> tmplst = compounds.getOrDefault(noun1, new ArrayList<String>());
					tmplst.add(noun2);
					compounds.put(noun1,tmplst);

				}

			}

			System.out.println("=======================");
			for(Node n : nodeList){

				boolean typeOf = false;
				
				
				String url = "http://words.bighugelabs.com/api/2/434d25a08183ba1a3ece937e2f1a50fc/"+ n.noun +"/json";
				URL obj = new URL(url);
				
				HttpURLConnection con = (HttpURLConnection) obj.openConnection();
				con = (HttpURLConnection) obj.openConnection();
				
				con.setRequestMethod("GET");

				//add request header
				con.setRequestProperty("User-Agent", "Mozilla/5.0");
				
				int reponseCode = con.getResponseCode();
				
				BufferedReader in = new BufferedReader(new InputStreamReader(con.getInputStream()));
				
				String inputLine;
				StringBuffer response = new StringBuffer();

				while ((inputLine = in.readLine()) != null) {
					response.append(inputLine);
				}
				in.close();

				// System.out.println(response.toString());

				JSONObject obj1 = new JSONObject(response.toString());
//				JSONObject obj2 = new JSONObject(obj1.get("noun"));
				
				JSONArray nounArray = obj1.getJSONObject("noun").getJSONArray("syn");
				
				System.out.println("printing.....");
				
				System.out.println(nounArray);
				
				for(int i=0; i<nounArray.length(); i++){
					if(foodset.contains(nounArray.get(i))){
						typeOf = true;
					}
				}

				if(foodset.contains(n.noun) || typeOf){

					System.out.print("food -->" + negation + " " + n.opinion + " ");

					if(compounds.containsKey(n.noun)){
						List<String> tmplst = compounds.get(n.noun);

						for(String s : tmplst)
							System.out.print(s + " ");
					}

					System.out.println(n.noun +" "+ n.polarity);

				}

				else if(serviceset.contains(n.noun))
					System.out.println("service -->" + n.opinion + " " + n.noun + " "+ n.polarity);

				else if(ambienceset.contains(n.noun))
					System.out.println("ambience -->" + n.opinion + " " + n.noun + " "+ n.polarity);

			}
			System.out.println("\n=======================");

		}
	}
}
