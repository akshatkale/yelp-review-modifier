import java.io.IOException;
import java.sql.Connection;  // for standard JDBC programs
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.LinkedHashMap;
// for BigDecimal and BigInteger support
import java.util.List;


public class Reviews {

	public static void main(String args[]) throws IOException{
		
		SentenceParser parser = new SentenceParser();
		Test testObj = new Test();
		DependencyParserDemo dp = new DependencyParserDemo();
		
		LinkedHashMap<String,Integer> nounMap = new LinkedHashMap<>();
		
		Test t = new Test();
		String [] arg={};
		
		List<String> reviews = new ArrayList<String>();

		
		String name = "Marigold Maison";

		try{

			Connection con = DriverManager.getConnection("jdbc:mysql://localhost:3306/yelpproject", "root", "root");

			Statement stmt = con.createStatement();
			
			
			String query = "SELECT review, restid from reviews "
					+ "WHERE restid = (SELECT restid from restaurants WHERE name = '"+ name +"');";
			
			ResultSet res = stmt.executeQuery(query);
			

			while(res.next()){
				reviews.add(res.getString("review").trim());

			}

		}catch(Exception e){
			
			System.out.println(e.getMessage());
		}
		
		
		for(String review : reviews){
			
			dp.getDependency(review);
			System.out.println("+++++++++++++++++++++++++++++++++++++++++++++");
			
		}
		
		nounMap = dp.getnounMap();
		
		
		List<Node> nodeList = new ArrayList<Node>();
		
		System.out.println("\n");
		System.out.println("node data is as follows:\n");
		for(Node n : nodeList){
			System.out.println(n.noun + " " + n.opinion);
		}
		
	}

}
